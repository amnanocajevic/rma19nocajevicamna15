package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

public class Odgovor implements Parcelable {
    String tekstOdgovora;
    boolean tacan;

    public Odgovor(String tekstOdgovora, boolean tacan) {
        this.tekstOdgovora = tekstOdgovora;
        this.tacan = tacan;
    }

    protected Odgovor(Parcel in) {
        tekstOdgovora = in.readString();
        tacan = in.readByte() != 0;
    }

    public static final Creator<Odgovor> CREATOR = new Creator<Odgovor>() {
        @Override
        public Odgovor createFromParcel(Parcel in) {
            return new Odgovor(in);
        }

        @Override
        public Odgovor[] newArray(int size) {
            return new Odgovor[size];
        }
    };

    public String getTekstOdgovora() {
        return tekstOdgovora;
    }

    public void setTekstOdgovora(String tekstOdgovora) {
        this.tekstOdgovora = tekstOdgovora;
    }

    public boolean isTacan() {
        return tacan;
    }

    public void setTacan(boolean tacan) {
        this.tacan = tacan;
    }

    @Override
    public String toString() {
        return tekstOdgovora;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(tekstOdgovora);
        dest.writeByte((byte) (tacan ? 1 : 0));
    }
}
