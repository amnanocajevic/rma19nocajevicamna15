package ba.unsa.etf.rma.fragmenti;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class InformacijeFrag extends Fragment {

    Kviz trenutniKviz;
    public static TextView nazivKviza;
    public static TextView brojTacnihPitanja;
    public static TextView brojPreostalihPitanja;
    public static TextView procenatTacnih;
    public static int brTacnihPitanja;
    public static int brPreostalihPitanja;
    public static float prTacnih;

    public void setTrenutniKviz(Kviz trenutniKviz) {
        this.trenutniKviz = trenutniKviz;
    }

    public void setNazivKviza(String nazivKviza) {
        if(this.nazivKviza!=null)
            this.nazivKviza.setText(nazivKviza);
}

    public void setBrojTacnihPitanja(String brojTacnihPitanja) {
        if(this.brojTacnihPitanja!=null)
        this.brojTacnihPitanja.setText(brojTacnihPitanja);
    }

    public void setBrojPreostalihPitanja(String brojPreostalihPitanja) {
        if(this.brojPreostalihPitanja!=null)
        this.brojPreostalihPitanja.setText(brojPreostalihPitanja);
    }

    public void setProcenatTacnih(String procenatTacnih) {
        if(this.procenatTacnih!=null)
        this.procenatTacnih.setText(procenatTacnih);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.informacije_frag, container, false);

        nazivKviza = view.findViewById(R.id.infNazivKviza);
        brojTacnihPitanja = view.findViewById(R.id.infBrojTacnihPitanja);
        brojPreostalihPitanja = view.findViewById(R.id.infBrojPreostalihPitanja);
        procenatTacnih = view.findViewById(R.id.infProcenatTacnih);

        if(IgrajKvizAkt.kviz!=null){
            String nazivvKviza = IgrajKvizAkt.kviz.getNaziv();
            nazivKviza.setText(nazivvKviza);
            brojTacnihPitanja.setText(String.valueOf(PitanjeFrag.brojTacnih));
            brojPreostalihPitanja.setText(String.valueOf(IgrajKvizAkt.kviz.getPitanja().size()- PitanjeFrag.brojOdgovorenih));
            String procenat;
            if(PitanjeFrag.brojOdgovorenih==0)procenat="100";
            else procenat = String.valueOf(100*PitanjeFrag.brojTacnih/PitanjeFrag.brojOdgovorenih);
            procenat += "%";
            procenatTacnih.setText(procenat);
        }



        Button btnKraj = (Button)view.findViewById(R.id.btnKraj);
        btnKraj.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), KvizoviAkt.class);
                startActivity(intent);
            }
        });
        return view;
    }
    public static void refresh(){
        String nazivvKviza = IgrajKvizAkt.kviz.getNaziv();

        nazivKviza.setText(nazivvKviza);

        brojTacnihPitanja.setText(String.valueOf(PitanjeFrag.brojTacnih));
        brojPreostalihPitanja.setText(String.valueOf(IgrajKvizAkt.kviz.getPitanja().size()- PitanjeFrag.brojOdgovorenih));
        String procenat;
        if(PitanjeFrag.brojOdgovorenih==0)procenat="100";
        else procenat = String.valueOf(100*PitanjeFrag.brojTacnih/PitanjeFrag.brojOdgovorenih);
        procenat += "%";
        procenatTacnih.setText(procenat);
    }

}
