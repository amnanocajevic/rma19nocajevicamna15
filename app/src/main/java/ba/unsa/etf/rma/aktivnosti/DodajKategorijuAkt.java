package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.os.AsyncTask;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;
import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AppStatus;
import ba.unsa.etf.rma.klase.Kategorija;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.db;

import ba.unsa.etf.rma.aktivnosti.DodajKvizAkt;
import ba.unsa.etf.rma.klase.Kviz;
import okhttp3.OkHttpClient;
import okhttp3.Request;

public class DodajKategorijuAkt extends AppCompatActivity implements IconDialog.Callback {
    private Icon[] selectedIcons;
     EditText dodajNaziv;
     EditText idIkone;
    boolean postojiVec = false;

    @Override
    public void onIconDialogIconsSelected(Icon[] icons) {
        EditText etIkona = findViewById(R.id.etIkona);
        selectedIcons = icons;
        etIkona.setText(String.valueOf(selectedIcons[0].getId()));
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kategoriju_akt);

        final IconDialog iconDialog = new IconDialog();

        Button button = findViewById(R.id.btnDodajIkonu);
        dodajNaziv = findViewById(R.id.etNaziv);
        idIkone = findViewById(R.id.etIkona);
        final Button dodajKategoriju = findViewById(R.id.btnDodajKategoriju);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) { // ovdje je rjeseno sta se desava s buttnom ikona
                iconDialog.setSelectedIcons(selectedIcons);
                iconDialog.show(getSupportFragmentManager(), "icon_dialog");
            }});

        TextWatcher loginTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                String inputNaziv = dodajNaziv.getText().toString().trim();
                String inputIkona = idIkone.getText().toString().trim();
                boolean postoji = false;

                if (dodajNaziv.getText().toString().trim().isEmpty()) {
                dodajNaziv.setBackgroundColor(ContextCompat.getColor(DodajKategorijuAkt.this, R.color.error));
                } else {
                dodajNaziv.setBackgroundColor(ContextCompat.getColor(DodajKategorijuAkt.this, R.color.element_pozadina));
                }

                //postoji je false ovdje
                for(Kategorija k : kategorije) {
                    if(k.getNaziv() != null && k.getNaziv().contains(inputNaziv)) {
                        postoji = true;
                    }
                }

                dodajKategoriju.setEnabled(!inputIkona.isEmpty() && !inputNaziv.isEmpty() && !postoji);
        }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };

        //ovo regulise kad se moze kliknuti na dugme dodaj kategoriju
        dodajNaziv.addTextChangedListener(loginTextWatcher);
        idIkone.addTextChangedListener(loginTextWatcher);

        dodajKategoriju.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(DodajKategorijuAkt.this).isOnline()) {
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("NAZIV_KAT", dodajNaziv.getText().toString());
                    db.insert("Kategorije", null, contentValues);
                    new KreirajDokumentKategorijeTask().execute("proba");
                    Intent intent = new Intent(v.getContext(),DodajKvizAkt.class);
                    intent.putExtra("naziv kategorije", dodajNaziv.getText().toString().trim()); // naziv kategorije
                    intent.putExtra("id", idIkone.getText().toString().trim()); // id kategorije
                    setResult(Activity.RESULT_OK, intent);
                    finish();

                } else {
                   alertDialogMaker("Niste spojeni na internet!");
                }

            }
        });

    }

    public class KreirajDokumentKategorijeTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid) {
          //  mojHandler.insertKategorija(dodajNaziv.getText().toString());
        }

        @Override
        protected Void doInBackground(String... strings){
            GoogleCredential credentials;
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                //dodavanje
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                String dokument = "{ \"fields\":  { \"naziv\" : {\"stringValue\" : \"" + dodajNaziv.getText().toString() +
                        "\"}, \"idIkonice\" : {\"integerValue\" : \"" + idIkone.getText().toString() +
                        "\"}}}";

                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input,0,input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor,"utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while((responseLine = br.readLine())!=null) {
                        response.append(responseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }

                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }
    }


    public class DohvatiDokumenteKategorijeTask extends AsyncTask<String, Void, Void> {
        @Override
        protected void onPostExecute(Void aVoid) {
            Intent intent = new Intent(getApplicationContext(),DodajKvizAkt.class);
            intent.putExtra("naziv kategorije", dodajNaziv.getText().toString().trim()); // naziv kategorije
            intent.putExtra("id", idIkone.getText().toString().trim()); // id kategorije
            if(!postojiVec) {
                new KreirajDokumentKategorijeTask().execute("proba");
                setResult(Activity.RESULT_OK, intent);
                finish();
            }

            else{
                alertDialogMaker("Unesena kategorija već postoji u bazi!");
            }



        }

        @Override
        protected Void doInBackground(String... strings){
            if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();

            GoogleCredential credentials;
            String query = null;
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                //dodavanje
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestProperty("Authorization", "Bearer " + TOKEN);

                InputStream in = new BufferedInputStream(conn.getInputStream());


                String rezultat = convertStreamToString(in).replaceAll("\\s+","");
                JSONObject jo = new JSONObject(rezultat);
                JSONArray dokumenti = jo.getJSONArray("documents");

                for(int i=0; i<dokumenti.length(); i++){
                    JSONObject dokument = dokumenti.getJSONObject(i);
                    JSONObject fields = dokument.getJSONObject("fields");
                    JSONObject naziv = fields.getJSONObject("naziv");
                    JSONObject id = fields.getJSONObject("idIkonice");
                    String nazivKategorije = naziv.getString("stringValue");
                    int idKategorije = id.getInt("integerValue");

                    if(nazivKategorije.equals(dodajNaziv.getText().toString())|| idKategorije == Integer.parseInt(idIkone.getText().toString()) )
                        postojiVec = true;
                }



                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }



    private void alertDialogMaker(String tekst) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(tekst);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    private boolean postojiUBazi() {
        return false;
    }



    }
