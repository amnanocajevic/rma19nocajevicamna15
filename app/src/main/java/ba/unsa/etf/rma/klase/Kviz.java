package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.Collections;

public class Kviz implements Parcelable {
    private String naziv;
    private ArrayList<Pitanje> pitanja;
    Kategorija kategorija;

    public Kviz(String naziv, ArrayList<Pitanje> pitanja, Kategorija kategorija) {
        this.naziv = naziv;
        this.pitanja = pitanja;
        this.kategorija = kategorija;
    }

    protected Kviz(Parcel in) {
        naziv = in.readString();
        pitanja = in.createTypedArrayList(Pitanje.CREATOR);
        kategorija = in.readParcelable(Kategorija.class.getClassLoader());
    }

    public static final Creator<Kviz> CREATOR = new Creator<Kviz>() {
        @Override
        public Kviz createFromParcel(Parcel in) {
            return new Kviz(in);
        }

        @Override
        public Kviz[] newArray(int size) {
            return new Kviz[size];
        }
    };

    @Override
    public String toString() {
        return naziv;
    }

    void dodajPitanje(Pitanje pitanje) {
        pitanja.add(pitanje);
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public ArrayList<Pitanje> getPitanja() {
        return pitanja;
    }

    public void setPitanja(ArrayList<Pitanje> pitanja) {
        this.pitanja = pitanja;
    }

    public Kategorija getKategorija() {
        return kategorija;
    }

    public void setKategorija(Kategorija kategorija) {
        this.kategorija = kategorija;
    }
    public ArrayList<Pitanje> dajRandomPitanja() {
        ArrayList<Pitanje> novaPitanja = new ArrayList<>();
        novaPitanja = pitanja;
        Collections.shuffle(novaPitanja);
        return novaPitanja;
    }
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeTypedList(pitanja);
        dest.writeParcelable(kategorija, flags);
    }
}
