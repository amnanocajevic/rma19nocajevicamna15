package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.List;

import ba.unsa.etf.rma.R;

public class CustomAdapterMogucaPitanja extends ArrayAdapter<Pitanje> {
    TextView naziv;
    IconView ikona;
    private int resourceLayout;
    private Context mContext;

    public CustomAdapterMogucaPitanja(Context context, int resource, List<Pitanje> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        Pitanje pitanje = getItem(position); // uzima jedan po jedan kviz

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        naziv = v.findViewById(R.id.nameL); // mjesto gdje ce nam se ispisati naziv kviza
        ikona = v.findViewById(R.id.iconL); // mjesto gdje ce biti ikona kviza

        naziv.setText(pitanje.getNaziv());
        ikona.setImageResource(R.drawable.slika2); //plusic ikona posto je dodajkviz


        return v;
    }
}
