package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.provider.ContactsContract;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.util.Range;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Array;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AppStatus;
import ba.unsa.etf.rma.klase.CustomAdapter;
import ba.unsa.etf.rma.klase.CustomAdapterMogucaPitanja;
import ba.unsa.etf.rma.klase.CustomAdapterPitanja;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Odgovor;
import ba.unsa.etf.rma.klase.Pitanje;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.dodajPitanje;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kvizovi;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.db;

//ENABLE BUTTON USLOVI (TACAN ODG I 1 NETACAN ODG)
//OPCENITO BTN DODAJ KVIZ

public class DodajKvizAkt extends AppCompatActivity {

    public static final int DODAJ_PITANJE_ACTION = 1337;
    public static final int DODAJ_KATEGORIJU_ACTION = 1500;
    public static final int IMPORT_KVIZ_ACTION = 1892;


    public static int brojPokretanja = 0;
    public static ArrayList<String> idSvihPitanjaKviza = new ArrayList<>();
    ArrayAdapter<Kategorija> spAdapter;
    CustomAdapterPitanja adapterPitanja;
    CustomAdapterMogucaPitanja adapterMogucihPitanja;
     ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
    final ArrayList<Pitanje> mogucaPitanja = new ArrayList<>();
    EditText nazivKviza ;
    Spinner spinner;




    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);


        if ( requestCode == DODAJ_PITANJE_ACTION && resultCode == Activity.RESULT_OK ) {
            // DodajPitanjeAkt zavrsila, i to potvrdno (nije samo back pritisnut npr)
            String nazivPitanja = intent.getStringExtra("naziv pitanja");
            ArrayList<String> odgovori = intent.getStringArrayListExtra("odgovori");
            String tacan = intent.getStringExtra("tacan");
            Pitanje temp = new Pitanje(nazivPitanja,nazivPitanja,odgovori,tacan);
            if(nazivPitanja!=null && !pitanjaKviza.contains(temp) ){
                pitanjaKviza.add(0,temp);
                adapterPitanja.notifyDataSetChanged();
            }
        }

        if ( requestCode == DODAJ_KATEGORIJU_ACTION && resultCode == Activity.RESULT_OK ) {
            String nazivKategorije = intent.getStringExtra("naziv kategorije");
            String id = intent.getStringExtra("id"); // id kategorije

            Kategorija nova = new Kategorija(nazivKategorije,id);
            kategorije.add(kategorije.size()-1,nova);
            spAdapter.notifyDataSetChanged();
            int spinnerPosition = spAdapter.getPosition(nova);
            spinner.setSelection(spinnerPosition);
        }


        if ( requestCode == IMPORT_KVIZ_ACTION && resultCode == Activity.RESULT_OK ) {
            boolean sveOk = true;
            Uri uri = null; //path do ucitanog dokumenta
            String linijeFajla = "";
            if (intent != null) {
                uri = intent.getData(); //iz intenta uzimamo taj path
                try {
                    if(uri!=null) // ako se nesto ucitalo
                    linijeFajla = readTextFromUri(uri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                String[] linije = linijeFajla.split("\n"); //sve linije
                String[] osnovnoKviz = linije[0].split(",");// prva s osnovnim podacima
                //0 naziv, 1 kategorija, 2 broj pitanja

                //ako je neispravno naveden broj pitanja
                for(int i = 0; i<kvizovi.size(); i++)
                    if(kvizovi.get(i).getNaziv().equals(osnovnoKviz[0])){
                        alertDialogMaker("Kviz kojeg importujete već postoji!");
                        sveOk = false;
                        break;
                    }

                //ako je neispravno naveden broj pitanja
                if(linije.length!=Integer.parseInt(osnovnoKviz[2])+1){
                    sveOk = false;
                    alertDialogMaker("Kviz kojeg imporujete ima neispravan broj pitanja!");
                }

               //prolazak kroz sva pitanja provjera broja odg i indeksa tacnog

                String[] dijeloviPitanja;
                if(sveOk)
                for(int i=1 ; i<=Integer.parseInt(osnovnoKviz[2]); i++ )  {
                    if(i<=linije.length) {
                        dijeloviPitanja = linije[i].split(",");
                        //linije: 0-osnovno , 1-pitanje 1, 2-pitanje 2 ....
                        //osnovno kviz: 0-naziv, 1-kategorija, 2-broj pitanja
                        //dijelovi pitanja: 0-tekst pitanja, 1-broj odgovora, 2-indeks tacnog odg, 3-odg 1, 4-odg 2 ....

                        if(Integer.parseInt(dijeloviPitanja[1])!=dijeloviPitanja.length-3) {
                            sveOk = false;
                            alertDialogMaker("Kviz kojeg importujete ima neispravan broj odgovora!");
                            break;
                        }

                        if(Integer.parseInt(dijeloviPitanja[2])<0 || Integer.parseInt(dijeloviPitanja[2])> Integer.parseInt(dijeloviPitanja[1])-1) {
                            sveOk = false;
                            alertDialogMaker("Kviz kojeg importujete ima neispravan index tačnog odgovora!");

                            break;
                        }

                    } else break;

                }
                //ako je proslo sve ovo bez alerta
                int i;
                if(sveOk){
                    for( i=0; i<kategorije.size(); i++) {
                        if(kategorije.get(i).getNaziv().equals(osnovnoKviz[1])) { //ako vec postoji kategorija
                            spinner.setSelection(i);
                            break;
                        }
                    }

                    if(i==kategorije.size()){//prosli cijelu petlju nismo nasli kategoriju
                        kategorije.add(kategorije.size()-1,new Kategorija(osnovnoKviz[1], "145"));
                        spinner.setSelection(kategorije.size()-2);
                    }

                    nazivKviza.setText(osnovnoKviz[0]);
                    //sad jos napravit pitanja
                    ArrayList<String> odgovoriTemp = new ArrayList<>();
                    ArrayList<Pitanje> pitanjaTemp = new ArrayList<>();
                    for(i=1 ; i<=Integer.parseInt(osnovnoKviz[2]); i++ )  {
                        dijeloviPitanja = linije[i].split(",");
                        for(int j=0; j<Integer.parseInt(dijeloviPitanja[1]); j++) {// prolazi onoliko koliko ima odg
                            odgovoriTemp.add(dijeloviPitanja[3+j]);
                        }

                        pitanjaTemp.add(new Pitanje(dijeloviPitanja[0],dijeloviPitanja[0],odgovoriTemp,odgovoriTemp.get(Integer.parseInt(dijeloviPitanja[2]))));
                    }

                    pitanjaKviza.addAll(pitanjaTemp);
                    adapterPitanja.notifyDataSetChanged();
                }
            }
        }
    }

    private void alertDialogMaker(String tekst) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(tekst);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_kviz_akt);

         Button btnDodajKviz = findViewById(R.id.btnDodajKviz);
        /*if(this.brojPokretanja==0)*/ pitanjaKviza.add(dodajPitanje);
        final Button btnImportKviz = findViewById(R.id.btnImportKviz);
        nazivKviza = findViewById(R.id.etNaziv);

        spAdapter =  new ArrayAdapter<>(this, R.layout.support_simple_spinner_dropdown_item, kategorije);
        spinner = findViewById(R.id.spKategorije);

        //doslo iz maina
        ArrayList<Pitanje> pitanjaOdabranogKviza = getIntent().getParcelableArrayListExtra("pitanja kviza");
        int indeksTrenutneKategorijeNaSpineru = getIntent().getIntExtra("kategorija",0);//
        String nazivOdabranogKviza = getIntent().getStringExtra("naziv kviza");
        String kategorijaOdabranogKviza = getIntent().getStringExtra("kategorija kviza");
        int i=0;
        //radnje ako vec postoji taj kviz pa ga editujemo aha
        if(!nazivOdabranogKviza.equals("Dodaj kviz") && nazivOdabranogKviza!=null)
            nazivKviza.setText(nazivOdabranogKviza);


        if(!pitanjaOdabranogKviza.isEmpty())
            pitanjaKviza=pitanjaOdabranogKviza;


        spinner.setAdapter(spAdapter);
        spAdapter.notifyDataSetChanged();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                //ako je u pitanju dodaj kategoriju otvara se nova aktivnost u suprotnom nista
                if(spinner.getSelectedItem().toString().equals("Dodaj kategoriju"))
                    startActivityForResult(new Intent(view.getContext(), DodajKategorijuAkt.class), DODAJ_KATEGORIJU_ACTION);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // U ADAPTER MOGUCIH PITANJA TREBA DA IDE POSEBNA LISTA
        ListView lvPitanja = findViewById(R.id.lvDodanaPitanja);
        adapterPitanja = new CustomAdapterPitanja(this, R.layout.customlistelement,pitanjaKviza);
        lvPitanja.setAdapter(adapterPitanja);
        final ListView lvMogucaPitanja = findViewById(R.id.lvMogucaPitanja);
        adapterMogucihPitanja = new CustomAdapterMogucaPitanja(this, R.layout.customlistelement, mogucaPitanja);
        lvMogucaPitanja.setAdapter(adapterMogucihPitanja);
        new DohvatiSvaPitanjaTask().execute("bla");


        lvPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                if(pitanjaKviza.get(position).getNaziv().equals("Dodaj pitanje")) {
                    Intent intent = new Intent(DodajKvizAkt.this, DodajPitanjeAkt.class);
                    intent.putExtra("kviz", nazivKviza.getText().toString());
                    startActivityForResult(intent, DODAJ_PITANJE_ACTION);
                }
                else {
                    mogucaPitanja.add((Pitanje) parent.getItemAtPosition(position));
                    pitanjaKviza.remove((Pitanje) parent.getItemAtPosition(position));
                    adapterMogucihPitanja.notifyDataSetChanged();
                }

            }
        });

        lvMogucaPitanja.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                pitanjaKviza.add((Pitanje) parent.getItemAtPosition(position));
                mogucaPitanja.remove((Pitanje) parent.getItemAtPosition(position));
                adapterPitanja.notifyDataSetChanged();
            }
        });

        btnDodajKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(DodajKvizAkt.this).isOnline()) {
                    Intent intent = new Intent(v.getContext(),KvizoviAkt.class);
                    intent.putExtra("naziv kviza", nazivKviza.getText().toString());
                    intent.putParcelableArrayListExtra("pitanja", new ArrayList<Pitanje>(pitanjaKviza.subList(0,pitanjaKviza.size()-1)));
                    intent.putExtra("kategorija", kategorije.get(spinner.getSelectedItemPosition()).getNaziv());
                    intent.putExtra("index", spinner.getSelectedItemPosition());

                    //dodavanje u sqlite
                    String idKategorije = sqliteSearch(kategorije.get(spinner.getSelectedItemPosition()).getNaziv());
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("NAZIV_KVIZ", nazivKviza.getText().toString());
                    contentValues.put("ID_KATEGORIJE", idKategorije);
                    db.insert("Kvizovi",null, contentValues);

                    if(Build.VERSION.SDK_INT >= 11/*HONEYCOMB*/) {
                        new KreirajDokumentKvizoviTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new KreirajDokumentKvizoviTask().execute();
                    }

                    //mojHandler.insertKviz(,nazivKviza.getText().toString())
                    setResult(Activity.RESULT_OK, intent);
                    finish();

                } else {
                    alertDialogMaker("Niste spojeni na internet!");
                }

            }

            private String sqliteSearch(String naziv) {
                Cursor mCursor = db.rawQuery(
                        "SELECT rowid  FROM Kategorije WHERE NAZIV_KAT= '"+naziv+"'" , null);
                if(mCursor.moveToFirst())
                return String.valueOf(mCursor.getInt(mCursor.getColumnIndex("rowid")));
                else
                    return null;
            }
        });


        for(i=0;i<kategorije.size();i++){
            if(kategorije.get(i).getNaziv().equals(kategorijaOdabranogKviza)){
                break;
            }
        }
        if(i!=kategorije.size() && !kategorije.get(i).getNaziv().equals("Dodaj kategoriju"))
            spinner.setSelection(i);

        if(nazivOdabranogKviza.equals("Dodaj kviz") && !kategorije.get(indeksTrenutneKategorijeNaSpineru).equals("Svi")) // odabrano je dodaj kviz iz neke kategorije
            spinner.setSelection(indeksTrenutneKategorijeNaSpineru);


        btnImportKviz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                intent.addCategory(Intent.CATEGORY_OPENABLE);
                intent.setType("text/*");
                startActivityForResult(intent, IMPORT_KVIZ_ACTION);
                //Intent.createChooser(intent, "Open CSV")
            }
        });

    }

    private String readTextFromUri(Uri uri) throws IOException {
        InputStream inputStream = getContentResolver().openInputStream(uri);
        BufferedReader reader = new BufferedReader(new InputStreamReader(
                inputStream));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = reader.readLine()) != null) {
            stringBuilder.append(line).append('\n');
        }
      //  fileInputStream.close();
       // parcelFileDescriptor.close();
        return stringBuilder.toString();
    }

    @Override
    protected void onStart() {
        super.onStart();
        //this.brojPokretanja++;
    }


    public class DohvatiSvaPitanjaTask extends AsyncTask<String, Void, Void> {

        ArrayList<Pitanje> pitanjaIzBaze = new ArrayList<>();

        @Override
        protected void onPostExecute(Void aVoid) {
            //filtrirati pitanja
           mogucaPitanja.addAll(pitanjaIzBaze);
           adapterMogucihPitanja.notifyDataSetChanged();

        }

        @Override
        protected Void doInBackground(String... strings) {
            if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();

            GoogleCredential credentials;
            ArrayList<String> odgovoriJednogPitanja = new ArrayList<>();
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                //dodavanje
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestProperty("Authorization", "Bearer " + TOKEN);

                InputStream in = new BufferedInputStream(conn.getInputStream());


                String rezultat = convertStreamToString(in).replaceAll("\\s+","");
                JSONObject jo = new JSONObject(rezultat);
                JSONArray dokumenti = jo.getJSONArray("documents");

                for(int i=0; i<dokumenti.length(); i++){
                    JSONObject dokument = dokumenti.getJSONObject(i);
                    JSONObject fields = dokument.getJSONObject("fields");
                    JSONObject naziv = fields.getJSONObject("naziv");
                    JSONObject index = fields.getJSONObject("indexTacnog");
                    //JSONArray jodgovori = fields.getJSONArray("odgovori");
                    JSONArray jodgovori = fields.getJSONObject("odgovori").getJSONObject("arrayValue").getJSONArray("values");
                    String tekstPitanja = naziv.getString("stringValue");
                    int indexTacnog = index.getInt("integerValue");

                    for(int j=0; j<jodgovori.length(); j++){
                        JSONObject odgovor = jodgovori.getJSONObject(j);
                        String odg = odgovor.getString("stringValue");
                        odgovoriJednogPitanja.add(odg);
                    }

                    //pod pretpostavkom da se vec ucitao kviz prije iz baze i unutra su pitanja
                    Pitanje temp = new Pitanje(tekstPitanja,tekstPitanja,odgovoriJednogPitanja,odgovoriJednogPitanja.get(indexTacnog));
                    if(!pitanjaKviza.contains(temp))
                         pitanjaIzBaze.add(temp);
                    odgovoriJednogPitanja.clear();
                }

                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    String pitanjaUId() {
        return null;
    }

    public class KreirajDokumentKvizoviTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings){
            GoogleCredential credentials;
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                //dodavanje
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");


                String dokument = "{ \"fields\":  { \"pitanja\": { \"arrayValue\": { \"values\" : [" ;
                for(int i=0; i<pitanjaKviza.size(); i++){
                    dokument+="{ \"stringValue\" : \"" + pitanjaKviza.get(i).getTekstPitanja() + "\"}";
                    if(i!=pitanjaKviza.size()-1)
                        dokument+=",";
                }

                dokument+="]}}, \"naziv\" : { \"stringValue\": \"" + nazivKviza.getText().toString() + "\"}," +
                        "\"idKategorije\" : { \"stringValue\" : \"" + ((Kategorija) spinner.getSelectedItem()).getId() +"\" }},}";

                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input,0,input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor,"utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while((responseLine = br.readLine())!=null) {
                        response.append(responseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }

                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }

    }
}