package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.List;
import java.util.Objects;

import ba.unsa.etf.rma.R;

public class KategorijaAdapter extends ArrayAdapter<Kategorija> {
    TextView naziv;
    IconView ikona;
    private int resourceLayout;
    private Context mContext;


    public KategorijaAdapter(Context context, int resource, List<Kategorija> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        Kategorija kat = getItem(position); // uzima jedan po jedan kviz

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        naziv = v.findViewById(R.id.nameL); // mjesto gdje ce nam se ispisati naziv kviza
        ikona = v.findViewById(R.id.iconL); // mjesto gdje ce biti ikona kviza

        if(Objects.equals(kat.getNaziv(), "Dodaj kategoriju"))  {
            naziv.setText(kat.getNaziv());
            ikona.setImageResource(R.drawable.slika2); //plusic ikona posto je dodajkviz
            //ikona.setIcon(671);
        }
        else if(kat!=null)  {

            if(naziv!=null)
                naziv.setText(kat.getNaziv()); //ispisat ce naziv kviza
            if(ikona!=null)
                ikona.setIcon(Integer.parseInt(kat.getId())); //postavit ce ikonu kategorije
        }

        return v;
    }
}
