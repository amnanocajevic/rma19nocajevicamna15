package ba.unsa.etf.rma.klase;

import android.os.Parcel;
import android.os.Parcelable;

import com.maltaisn.icondialog.Icon;
import com.maltaisn.icondialog.IconDialog;

import java.io.Serializable;

public class Kategorija implements Parcelable {
    private String naziv;
    private String id;
    private Icon ikona;


    public Kategorija(String naziv, String id) {
        this.naziv = naziv;
        this.id = id;
    }

    public Kategorija(String naziv, String id, Icon ikona) {
        this.naziv = naziv;
        this.id = id;
        this.ikona = ikona;
    }

    protected Kategorija(Parcel in) {
        naziv = in.readString();
        id = in.readString();
    }

    public static final Creator<Kategorija> CREATOR = new Creator<Kategorija>() {
        @Override
        public Kategorija createFromParcel(Parcel in) {
            return new Kategorija(in);
        }

        @Override
        public Kategorija[] newArray(int size) {
            return new Kategorija[size];
        }
    };

    @Override
    public String toString() {
        return naziv;
    }

    public String getNaziv() {
        return naziv;
    }

    public void setNaziv(String naziv) {
        this.naziv = naziv;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Icon getIkona() {
        return ikona;
    }

    public void setIkona(Icon ikona) {
        this.ikona = ikona;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(naziv);
        dest.writeString(id);
    }
}
