package ba.unsa.etf.rma.aktivnosti;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.database.sqlite.SQLiteDatabase;
import android.os.AsyncTask;
import android.os.Handler;
import android.provider.CalendarContract;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.GestureDetector;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.concurrent.TimeUnit;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.klase.CustomAdapter;
import ba.unsa.etf.rma.klase.CustomAdapterSpinnerMain;
import ba.unsa.etf.rma.klase.DatabaseHandler;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.Pitanje;

public class KvizoviAkt extends AppCompatActivity {

    public static SQLiteDatabase db;
    public static final int DODAJ_KVIZ_ACTION = 1111;
    private static final int MY_CAL_REQ = 22222222 ;
    public static int brojPokretanja = 0;

    //adapteri
    CustomAdapter adapter;
    ArrayAdapter<Kategorija> spAdapter;
    //reference
    Spinner spinner;
    ListView lista;
    private ListaFrag lijeviFrag;
    private DetailFrag desniFrag;

    //kontejneri svega
    public static ArrayList<Kviz> kvizovi = new ArrayList<>();
    public static ArrayList<Kategorija> kategorije = new ArrayList<>();
    // ArrayList<Kategorija> kategorije = new ArrayList<Kategorija>(){{ new Kategorija("Svi",String.valueOf(Integer.MAX_VALUE));}};
    static ArrayList<Pitanje> pitanja = new ArrayList<>();

    public Kategorija svi = new Kategorija("Svi", "0"); // ona mora biti, uzela sam da mi je id kategorije zadnji int da se ne clasha
    public Kategorija dodajKategorija = new Kategorija("Dodaj kategoriju", "1"); //a i ova mora biti, nema je u svim kategorijama jer je fake
    public Kviz dodajKviz = new Kviz("Dodaj kviz", new ArrayList<Pitanje>(), dodajKategorija);
    static public Pitanje dodajPitanje = new Pitanje("Dodaj pitanje", "Dodaj pitanje", new ArrayList<String>(), "");

    GestureDetector gestureDetector;
    Handler handler;

    private void alertDialogMaker(String tekst) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(tekst);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);


        if (requestCode == DODAJ_KVIZ_ACTION && resultCode == Activity.RESULT_OK) {
            // DodajKVIZAkt zavrsila, i to potvrdno (nije samo back pritisnut npr)
            String nazivKviza = intent.getStringExtra("naziv kviza");
            ArrayList<Pitanje> pitanja = intent.getParcelableArrayListExtra("pitanja");
            // ovo popraviti
            String kategorija = intent.getStringExtra("kategorija");
            int idKategorije= intent.getIntExtra("index", 0);

            ///////////////////
            Kviz temp = new Kviz(nazivKviza, pitanja, kategorije.get(idKategorije));
            boolean postojiKviz = false;
            int pozicija=0;
            for(int i=0;i<kvizovi.size();i++){
                if(kvizovi.get(i).getNaziv().equals(nazivKviza)){
                    postojiKviz=true;
                    pozicija=i;
                }
            }
            if(postojiKviz){
                kvizovi.set(pozicija,temp);
            }
            else kvizovi.add(temp);
            adapter.notifyDataSetChanged();
            spinner.setSelection(idKategorije);
        }
    }


    private void initializeViews(Context context) {

     //  gestureDetector = new GestureDetector(KvizoviAkt.this, handler);
        //postavljene reference


        spinner = findViewById(R.id.spPostojeceKategorije);
        if(spinner==null)
            return;
        lista = findViewById(R.id.lvKvizovi);
        if(lista==null)
            return;

        //dodani obavezni elementi
        if(brojPokretanja==0){
            kategorije.add(svi);
            Kategorija test = new Kategorija("Test", "26");
            Pitanje testno = new Pitanje("Testno", "Testno", new ArrayList<String>(),  " ");
            ArrayList<Pitanje> pitanja =  new ArrayList<Pitanje>();
            ArrayList<String> eOdgovori = new ArrayList<>(); eOdgovori.add("haha");eOdgovori.add("hahaha");eOdgovori.add("hehe");

            pitanja.add(new Pitanje("Testno", "Testno", eOdgovori, "hehe"));
            pitanja.add(new Pitanje("Da ima vise", "Da ima vise", eOdgovori, "hehe"));
            pitanja.add(new Pitanje("Treba mi puno pitanja :c", "Treba mi puno pitanja :c", eOdgovori, "haha"));
            pitanja.add(new Pitanje("Meow", "Meow", eOdgovori, "hahaha"));
            pitanja.add(new Pitanje("Neparno test", "Nepano test", eOdgovori, "hahaha"));

            Kviz testKviz = new Kviz("Testni", pitanja, test);
            kategorije.add(test);
            kvizovi.add(testKviz);

            kategorije.add(dodajKategorija);
            kvizovi.add(dodajKviz);
            pitanja.add(dodajPitanje);
            this.brojPokretanja++;
        }

        //spinner

        spAdapter = new CustomAdapterSpinnerMain(this, R.layout.support_simple_spinner_dropdown_item, kategorije, 1);
        spAdapter.registerDataSetObserver(new DataSetObserver() { //ako element iz spinnera bude obrisan(kategorija)
            @Override
            public void onChanged() {
                super.onChanged();
            }
        });

        if(spinner!=null){
            spinner.setAdapter(spAdapter);

            spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    if (position >= 0 && position < kategorije.size()) {
                        dajPodatkeOOdabranojKategoriji(spinner.getItemAtPosition(position).toString());
                    } else {
                        Toast.makeText(KvizoviAkt.this, "Odabrana kategorija ne postoji!", Toast.LENGTH_SHORT).show();
                        // ovo se vrlo vjerovatno nikad nece ni desiti jer ne moze se kreirati prazna kategorija (bez ida)
                    }
                }
                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        }

        //sta se desava kad kliknemo na element liste
        //if(lista!=null)
        lista.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {
                //provjeriti kalendar, ako ima nesto u kalendaru vec zabraniti otvaranje intenta
                Kviz kviz = (Kviz) lista.getItemAtPosition(arg2);
                if(provjeraKalendaraOK(arg2) && !kviz.getNaziv().equals("Dodaj kviz") ) {
                    Intent intent = new Intent(KvizoviAkt.this, IgrajKvizAkt.class);
                    Bundle bundle = new Bundle();
                    bundle.putParcelable("kviz",adapter.getItem(arg2));
                    intent.putExtras(bundle);
                    startActivity(intent); //ne treba nikakav rezultat vratit hajde klikni na kviz
                }
            }
        });


        if(lista!=null) //dugi klik na element liste
            lista.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> arg0, View arg1,int pos, long id) {
                    Intent intent = new Intent(arg1.getContext(), DodajKvizAkt.class);
                    intent.putExtra("kategorija", spinner.getSelectedItemPosition()); // da znamo kategoriju
                    Kviz item = (Kviz) adapter.getItem(pos); // unutra treba da bude sve potrebnoza dalje
                    ArrayList<Pitanje> pitanjes = item.getPitanja();
                    intent.putExtra("naziv kviza", item.getNaziv());
                    intent.putExtra("kategorija kviza", item.getKategorija().getNaziv());
                    intent.putParcelableArrayListExtra("pitanja kviza", pitanjes);
                    startActivityForResult(intent, DODAJ_KVIZ_ACTION);
                    return true;
                }
            });
    }

    public boolean provjeraKalendaraOK(int position) {

        Kviz kviz = (Kviz) lista.getItemAtPosition(position);
        double doubleVarVrijeme = (double) kviz.getPitanja().size()/2;
        int intVarVrijeme = (int) (doubleVarVrijeme + 0.5);


        if (ContextCompat.checkSelfPermission(KvizoviAkt.this,
                Manifest.permission.READ_CALENDAR)
                != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            // Should we show an explanation?
            if (ActivityCompat.shouldShowRequestPermissionRationale(KvizoviAkt.this,
                    Manifest.permission.READ_CALENDAR)) {
                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.
            } else {
                // No explanation needed; request the permission
                ActivityCompat.requestPermissions(KvizoviAkt.this,
                        new String[]{Manifest.permission.READ_CALENDAR},
                        2000);

                // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                // app-defined int constant. The callback method gets the
                // result of the request.
            }
        } else {

            //podaci po kojima cemo pretrazivati
            //pocinjemo od sadasnjeg trenutka i idemo do potencijalnog pocetka kviza
            GregorianCalendar now = new GregorianCalendar();
            GregorianCalendar end = new GregorianCalendar();
            end.setTimeInMillis(now.getTimeInMillis()+ TimeUnit.MINUTES.toMillis(intVarVrijeme));

            //koje kolone uzimamo
            //ne treba nam nista vise osim pocetka i eventualno kraja eventa
            String [] projection =  {
                    CalendarContract.Events.DTSTART,
                    CalendarContract.Events.DTEND
            };

            //filtriranje redova
            String selection = "((dtstart >= ?) AND (dtstart <= ?))";
            //ove ce vrijednosti zamijeniti ?
            String [] arguments = { String.valueOf(now.getTimeInMillis()),
                                    String.valueOf(end.getTimeInMillis()) };

            Cursor cursor = (Cursor) getContentResolver().query(CalendarContract.Events.CONTENT_URI,projection,selection, arguments,null);
            while(cursor.moveToNext()) {
                if(cursor!=null){
                    int vrijemePocetak = cursor.getColumnIndex(CalendarContract.Events.DTSTART);
                    int vrijemeKraj = cursor.getColumnIndex(CalendarContract.Events.DTEND);

                    long vrijemePocetakValue = cursor.getLong(vrijemePocetak);
                    long vrijemeKrajValue = cursor.getLong(vrijemeKraj);

                    //podaci o dogadaju iz kalendara
                    GregorianCalendar dtPocetakEventa = new GregorianCalendar();
                    dtPocetakEventa.setTimeInMillis(vrijemePocetakValue);
                    GregorianCalendar dtKrajEventa = new GregorianCalendar();
                    dtKrajEventa.setTimeInMillis(vrijemeKrajValue);

                    //ovdje ce u milisekundama ostati koliko treba vremena do kviza
                    GregorianCalendar temp = new GregorianCalendar();
                    temp.setTimeInMillis(dtPocetakEventa.getTimeInMillis()-now.getTimeInMillis());

                    alertDialogMaker("Imate događaj u kalendaru za " + TimeUnit.MILLISECONDS.toMinutes(temp.getTimeInMillis()) + " minuta");
                    return false;

                }
            }
        }

        return true;
    }

    private List<Kviz> dajKvizove() { // vraca sve kvizove koji postoje
        List<Kviz> kvizoviPomocni = new ArrayList<>();
        kvizoviPomocni.clear();
        kvizoviPomocni.addAll(kvizovi);
        return kvizoviPomocni;
    }

    private void dajPodatkeOOdabranojKategoriji(String kategorija) {
        ArrayList<Kviz> kvizovi = new ArrayList<>(); //lista kvizova

        if (kategorija.equals("Svi")) { // dobit cu popis svih kvizova pa i fake dodaj kviz element
            adapter = new CustomAdapter(this, R.layout.customlistelement, this.dajKvizove());
        } else { // neka druga kategorija
            for (Kviz kviz : dajKvizove()) {
                if (kviz.getKategorija().getNaziv().equals(kategorija) || kviz.getNaziv() == "Dodaj kviz") // vraca kategoriju i fake dodaj kviz element
                    kvizovi.add(kviz);
            }
            adapter = new CustomAdapter(this, R.layout.customlistelement, kvizovi);
        }
        lista.setAdapter(adapter);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_kvizovi_akt);
        initializeViews(KvizoviAkt.this);

        if(spinner==null) {
            lijeviFrag = new ListaFrag();
            desniFrag = new DetailFrag();

            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();

        /*getFragmentManager().beginTransaction()
                .replace(R.id.listPlace, lijeviFrag)
              //  .replace(R.id.detailPlace, desniFrag)//momenat oki
                .commit();*/
            ft.replace(R.id.listPlace,lijeviFrag);
            ft.replace(R.id.detailPlace,desniFrag);
            ft.commit();
        }

        db = new DatabaseHandler(this).getWritableDatabase();
        new DohvatiSvaPitanjaTask().execute("bla");
        new DohvatiSveKategorijeTask().execute("bla");

      //  new DohvatiSveKvizoveTask().execute("bla");
        //https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Kategorije?access_token=ya29.c.EloUBzISnvFjUIFoGtXk6teYjjYEqdzQocdfXxv0QB2TRKJ7XUQRDUjLPoknaeHZRu67jwQHsOAV7_K3fwznWVINDuTvQwxT3-kdoRFFVeaYQPmOZFvCg5r2w4U

    }
    public class DohvatiSveKvizoveTask extends AsyncTask<String, Void, Void> {

        ArrayList<Kviz> kvizoviIzBaze = new ArrayList<>();

        @Override
        protected void onPostExecute(Void aVoid) {
            //filtrirati pitanja
            //popuniPitanjaKvizovimaIzBaze(kvizoviIzBaze);
            kvizovi.addAll(kvizoviIzBaze);
            adapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(String... strings) {
            if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();

            GoogleCredential credentials;
            ArrayList<Pitanje> nepotpunaPitanja = new ArrayList<>();
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                //dodavanje
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Kvizovi?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestProperty("Authorization", "Bearer " + TOKEN);

                InputStream in = new BufferedInputStream(conn.getInputStream());


                String rezultat = convertStreamToString(in).replaceAll("\\s+","");
                JSONObject jo = new JSONObject(rezultat);
                JSONArray dokumenti = jo.getJSONArray("documents");

                for(int i=0; i<dokumenti.length(); i++){
                    JSONObject dokument = dokumenti.getJSONObject(i);
                    JSONObject fields = dokument.getJSONObject("fields");

                    JSONObject naslov = fields.getJSONObject("naziv");
                    JSONObject kategorija = fields.getJSONObject("idKategorije");
                    //JSONArray jodgovori = fields.getJSONArray("odgovori");
                    JSONArray jpitanja = fields.getJSONObject("pitanja").getJSONObject("arrayValue").getJSONArray("values");
                    String nazivKviza = naslov.getString("stringValue");
                    String idKategorije = kategorija.getString("stringValue");

                    for(int j=0; j<jpitanja.length(); j++){
                        JSONObject pitanje = jpitanja.getJSONObject(j);
                        String jednoPitanjeKviza = pitanje.getString("stringValue");
                        nepotpunaPitanja.add(new Pitanje(jednoPitanjeKviza,jednoPitanjeKviza,null,null));

                    }

                    kvizoviIzBaze.add(new Kviz(nazivKviza,nepotpunaPitanja,pronadiKategorijuPoIdu(idKategorije)));

                }

                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public ArrayList<Pitanje> pronadiPitanjaPoTekstu (ArrayList<Pitanje> nepotpunaPitanja) {
        ArrayList<Pitanje> potpunaPitanja = new ArrayList<>();

        for(int i=0; i<nepotpunaPitanja.size(); i++){
            for(int j=0; j<pitanja.size(); j++) {
                if(nepotpunaPitanja.get(i).getNaziv().equals(pitanja.get(j).getTekstPitanja()))
                    potpunaPitanja.add(pitanja.get(j));
            }
        }

        return potpunaPitanja;
    }

    public Kategorija pronadiKategorijuPoIdu(String id) {
        Kategorija povratna = null;
        for(int i=0; i<kategorije.size();i++)
            if(kategorije.get(i).getId().equals(id))
                povratna=kategorije.get(i);
        return povratna;
    }

    public class DohvatiSvaPitanjaTask extends AsyncTask<String, Void, Void> {

        ArrayList<Pitanje> pitanjaIzBaze = new ArrayList<>();

        @Override
        protected void onPostExecute(Void aVoid) {
            //filtrirati pitanja
            pitanja.addAll(pitanjaIzBaze);
        }

        @Override
        protected Void doInBackground(String... strings) {
            if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();

            GoogleCredential credentials;
            ArrayList<String> odgovoriJednogPitanja = new ArrayList<>();
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                //dodavanje
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestProperty("Authorization", "Bearer " + TOKEN);

                InputStream in = new BufferedInputStream(conn.getInputStream());


                String rezultat = convertStreamToString(in).replaceAll("\\s+","");
                JSONObject jo = new JSONObject(rezultat);
                JSONArray dokumenti = jo.getJSONArray("documents");

                for(int i=0; i<dokumenti.length(); i++){
                    JSONObject dokument = dokumenti.getJSONObject(i);
                    JSONObject fields = dokument.getJSONObject("fields");
                    JSONObject naziv = fields.getJSONObject("naziv");
                    JSONObject index = fields.getJSONObject("indexTacnog");
                    //JSONArray jodgovori = fields.getJSONArray("odgovori");
                    JSONArray jodgovori = fields.getJSONObject("odgovori").getJSONObject("arrayValue").getJSONArray("values");
                    String tekstPitanja = naziv.getString("stringValue");
                    int indexTacnog = index.getInt("integerValue");

                    for(int j=0; j<jodgovori.length(); j++){
                        JSONObject odgovor = jodgovori.getJSONObject(j);
                        String odg = odgovor.getString("stringValue");
                        odgovoriJednogPitanja.add(odg);
                    }

                    Pitanje temp = new Pitanje(tekstPitanja,tekstPitanja,odgovoriJednogPitanja,odgovoriJednogPitanja.get(indexTacnog));
                    pitanjaIzBaze.add(temp); //ovdje nema veze u kojem je kvizu samo dohvata sva pitanja
                    odgovoriJednogPitanja.clear();
                }

                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public String convertStreamToString(InputStream is) {
        BufferedReader reader = new BufferedReader(new
                InputStreamReader(is));
        StringBuilder sb = new StringBuilder();
        String line = null;
        try {
            while ((line = reader.readLine()) != null) {
                sb.append(line + "\n");
            }
        } catch (IOException e) {
        } finally {
            try {
                is.close();
            } catch (IOException e) {
            }
        }
        return sb.toString();
    }

    public class DohvatiSveKategorijeTask extends AsyncTask<String, Void, Void> {

        ArrayList<Kategorija> kategorijeIzBaze = new ArrayList<>();

        @Override
        protected void onPostExecute(Void aVoid) {
            kategorije.addAll(1,kategorijeIzBaze); //dodaje se poslije svi
            spAdapter.notifyDataSetChanged();
        }

        @Override
        protected Void doInBackground(String... strings){
            if(android.os.Debug.isDebuggerConnected())
                android.os.Debug.waitForDebugger();

            GoogleCredential credentials;
            String query = null;
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();

                //dodavanje
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Kategorije?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setRequestProperty("Authorization", "Bearer " + TOKEN);

                InputStream in = new BufferedInputStream(conn.getInputStream());


                String rezultat = convertStreamToString(in).replaceAll("\\s+","");
                JSONObject jo = new JSONObject(rezultat);
                JSONArray dokumenti = jo.getJSONArray("documents");

                for(int i=0; i<dokumenti.length(); i++){
                    JSONObject dokument = dokumenti.getJSONObject(i);
                    JSONObject fields = dokument.getJSONObject("fields");
                    JSONObject naziv = fields.getJSONObject("naziv");
                    JSONObject id = fields.getJSONObject("idIkonice");
                    String nazivKategorije = naziv.getString("stringValue");
                    int idKategorije = id.getInt("integerValue");
                    kategorijeIzBaze.add(new Kategorija(nazivKategorije,String.valueOf(idKategorije)));
                }

                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
    }


    @Override
    protected void onStart() {
        super.onStart();
        this.brojPokretanja++;
    }
}