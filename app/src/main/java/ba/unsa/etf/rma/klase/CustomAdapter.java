package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import com.maltaisn.icondialog.IconView;
import java.util.List;
import java.util.Objects;

import ba.unsa.etf.rma.R;

public class CustomAdapter extends ArrayAdapter<Kviz> implements Filterable {

    TextView naziv;
    IconView ikona;
    private int resourceLayout;
    private Context mContext;

    public CustomAdapter(Context context, int resource, List<Kviz> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        Kviz kviz = getItem(position); // uzima jedan po jedan kviz

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        naziv = v.findViewById(R.id.nameL); // mjesto gdje ce nam se ispisati naziv kviza
        ikona = v.findViewById(R.id.iconL); // mjesto gdje ce biti ikona kviza

        if(Objects.equals(kviz.getNaziv(), "Dodaj kviz"))  {
            naziv.setText(kviz.getNaziv());
            ikona.setImageResource(R.drawable.slika2); //plusic ikona posto je dodajkviz
            //ikona.setIcon(671);
        }
        else if(kviz!=null)  {

                if(naziv!=null)
                    naziv.setText(kviz.getNaziv()); //ispisat ce naziv kviza
                if(ikona!=null)
                    ikona.setIcon(Integer.parseInt(kviz.getKategorija().getId())); //postavit ce ikonu kategorije
        }

        return v;
    }
}