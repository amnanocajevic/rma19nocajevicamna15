package ba.unsa.etf.rma.aktivnosti;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.os.AsyncTask;
import android.os.Build;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;

import com.google.api.client.googleapis.auth.oauth2.GoogleCredential;
import com.google.common.collect.Lists;

import org.json.JSONArray;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.concurrent.Executor;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.AppStatus;
import ba.unsa.etf.rma.klase.CustomAdapter;
import ba.unsa.etf.rma.klase.CustomAdapterOdgovor;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.Odgovor;
import ba.unsa.etf.rma.klase.Pitanje;

import static ba.unsa.etf.rma.aktivnosti.DodajKvizAkt.idSvihPitanjaKviza;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.db;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;
import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.pitanja;


public class DodajPitanjeAkt extends AppCompatActivity {
    EditText etNaziv;
    ArrayList<Odgovor> odgovori;
    boolean imaTacan = false;
    boolean imaOdgovor = false;
    Odgovor tacan;
    String nazivKviza;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dodaj_pitanje_akt);

        ListView lvOdgovori = findViewById(R.id.lvOdgovori);
         odgovori = new ArrayList<>(); // svi odgovori
        final CustomAdapterOdgovor adapterOdgovori = new CustomAdapterOdgovor(this, R.layout.customlistelement,odgovori);
        lvOdgovori.setAdapter(adapterOdgovori);

         etNaziv = findViewById(R.id.etNaziv);
        final EditText etOdgovor = findViewById(R.id.etOdgovor);
        Button btnDodajOdgovor = findViewById(R.id.btnDodajOdgovor);
        final Button btnDodajTacan = findViewById(R.id.btnDodajTacan);
        final Button btnDodajPitanje = findViewById(R.id.btnDodajPitanje);
        nazivKviza = getIntent().getStringExtra("kviz");


        //regulise ako nista nije upisano i kad ce se enableat dugme
        TextWatcher nazivTextWatcher = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {


                String tekstPitanja = etNaziv.getText().toString().trim();
                if (etNaziv.getText().toString().trim().isEmpty()) {
                    etNaziv.setBackgroundColor(ContextCompat.getColor(DodajPitanjeAkt.this, R.color.error));
                } else {
                    etNaziv.setBackgroundColor(ContextCompat.getColor(DodajPitanjeAkt.this, R.color.element_pozadina));
                }


                btnDodajPitanje.setEnabled(!etNaziv.getText().toString().isEmpty() && imaTacan && imaOdgovor);
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        };
        etNaziv.addTextChangedListener(nazivTextWatcher);

        btnDodajOdgovor.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Odgovor temp = new Odgovor(etOdgovor.getText().toString(), false);
                if(!odgovori.contains(temp))  {
                    odgovori.add(temp);
                    adapterOdgovori.notifyDataSetChanged();
                    imaOdgovor = true;
                }

                if(imaOdgovor && imaTacan && !etNaziv.getText().toString().trim().isEmpty())
                    btnDodajPitanje.setEnabled(true);
            }
        });

        btnDodajTacan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Odgovor temp = new Odgovor(etOdgovor.getText().toString(), true);
                if(!odgovori.contains(temp))  {
                    tacan = temp;
                    odgovori.add(temp);
                    adapterOdgovori.notifyDataSetChanged();
                    btnDodajTacan.setEnabled(false); // da se ne moze vise unijeti tacnih, jer je samo jedan tacan
                    imaTacan = true;
                }

                if(imaTacan && imaOdgovor && !etNaziv.getText().toString().trim().isEmpty())
                    btnDodajPitanje.setEnabled(true);

            }
        });


        btnDodajPitanje.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppStatus.getInstance(DodajPitanjeAkt.this).isOnline()) {
                    ArrayList<String> odgovoriString = new ArrayList<>();

                    for(Odgovor o : odgovori) {
                        odgovoriString.add(o.toString());
                    }
                    Intent intent = new Intent(v.getContext(),DodajKvizAkt.class);
                    intent.putExtra("naziv pitanja", etNaziv.getText().toString().trim());
                    intent.putStringArrayListExtra("odgovori", odgovoriString);
                    intent.putExtra("tacan", tacan.toString());

                    String idKviza = sqliteSearch();
                    ContentValues contentValues = new ContentValues();
                    contentValues.put("TEKST_PITANJA", etNaziv.getText().toString());
                    contentValues.put("ID_KVIZA", idKviza );
                    db.insert("Pitanja",null, contentValues);

                    //ovdje imam pitanje u bazi sad mogu dodavati odgovore

                    String idPitanja = sqliteSearchPitanje();

                    for(String odg : odgovoriString) {
                        ContentValues cv = new ContentValues();
                        cv.put("TEKST_ODG", odg);
                        if(odg==tacan.toString())
                            cv.put("JEL_TACAN", 1);
                        else
                            cv.put("JEL_TACAN", 0);
                        cv.put("ID_PITANJA", idPitanja);
                        db.insert("Odgovori", null, cv);
                    }


                    if(Build.VERSION.SDK_INT >= 11/*HONEYCOMB*/) {
                        new KreirajDokumentPitanjaTask().executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                    } else {
                        new KreirajDokumentPitanjaTask().execute();
                    }

                    setResult(Activity.RESULT_OK, intent);
                    finish();
                } else {
                    alertDialogMaker("Niste spojeni na internet!");
                }

            }

            private String sqliteSearch() {
                Cursor mCursor = db.rawQuery(
                        "SELECT rowid  FROM Kvizovi WHERE NAZIV_KVIZ= '"+nazivKviza+"'" , null);
                if(mCursor.moveToFirst())
                    return String.valueOf(mCursor.getInt(mCursor.getColumnIndex("rowid")));
                else
                    return null;
            }

            private String sqliteSearchPitanje() {
                Cursor mCursor = db.rawQuery(
                        "SELECT rowid  FROM Pitanja WHERE TEKST_PITANJA= '"+etNaziv.getText().toString()+"'" , null);
                if(mCursor.moveToFirst())
                    return String.valueOf(mCursor.getInt(mCursor.getColumnIndex("rowid")));
                else
                    return null;
            }

        });

    }

    private void alertDialogMaker(String tekst) {
        AlertDialog.Builder builder1 = new AlertDialog.Builder(this);
        builder1.setMessage(tekst);
        builder1.setCancelable(true);

        builder1.setPositiveButton(
                "OK",
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });


        AlertDialog alert11 = builder1.create();
        alert11.show();

    }

    public class KreirajDokumentPitanjaTask extends AsyncTask<String, Void, Void> {
        @Override
        protected Void doInBackground(String... strings){
            GoogleCredential credentials;
            try{
                InputStream tajnaStream = getResources().openRawResource(R.raw.secret);
                credentials = GoogleCredential.fromStream(tajnaStream).
                        createScoped(Lists.newArrayList("https://www.googleapis.com/auth/datastore"));
                credentials.refreshToken();
                String TOKEN = credentials.getAccessToken();
                Log.d("TOKEN: ",TOKEN);
                //dodavanje
                Random random = new Random();
                String url = "https://firestore.googleapis.com/v1/projects/rmaprojekat-c818a/databases/(default)/documents/Pitanja?access_token=";
                URL urlObj = new URL(url + URLEncoder.encode(TOKEN,"UTF-8"));
                //URL urlObj = new URL(url);
                HttpURLConnection conn = (HttpURLConnection) urlObj.openConnection();
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setRequestProperty("Accept", "application/json");

                ArrayList<String> odgovoriString = new ArrayList<>();

                for(Odgovor o : odgovori) {
                    odgovoriString.add(o.toString());
                }

                String dokument = "{\"fields\":  { \"naziv\" : {\"stringValue\": \"" + etNaziv.getText().toString() +
                        "\"}, \"odgovori\" : {\"arrayValue\" :  {\"values\" : [ " ;

                for(int i =0; i<odgovoriString.size(); i++) {
                    dokument+="{ \"stringValue\" : \"" + odgovoriString.get(i) + "\" }";
                    if(i!=odgovoriString.size()-1)   // nije zadnji odg
                        dokument+=",";
                }

                dokument+="]}}" + ", \"indexTacnog\" : {\"integerValue\" : \"" + odgovoriString.indexOf(tacan.toString());
                dokument+="\"}}}";



                try(OutputStream os = conn.getOutputStream()) {
                    byte[] input = dokument.getBytes("utf-8");
                    os.write(input,0,input.length);
                }

                int code = conn.getResponseCode();
                InputStream odgovor = conn.getInputStream();
                try(BufferedReader br = new BufferedReader(new InputStreamReader(odgovor,"utf-8"))) {
                    StringBuilder response = new StringBuilder();
                    String responseLine = null;
                    while((responseLine = br.readLine())!=null) {
                        response.append(responseLine.trim());
                    }
                    Log.d("ODGOVOR", response.toString());
                }

                Log.d("TOKEN",TOKEN);
            }catch(IOException e){
                e.printStackTrace();
            }
            return null;
        }

    }
}