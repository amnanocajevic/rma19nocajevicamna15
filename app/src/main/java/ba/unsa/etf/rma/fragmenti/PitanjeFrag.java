package ba.unsa.etf.rma.fragmenti;

import android.content.Context;
import android.icu.text.IDNA;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.IgrajKvizAkt;
import ba.unsa.etf.rma.aktivnosti.KvizoviAkt;
import ba.unsa.etf.rma.klase.Kviz;
import ba.unsa.etf.rma.klase.OdgovorAdapter;
import ba.unsa.etf.rma.klase.Pitanje;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PitanjeFrag.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PitanjeFrag#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PitanjeFrag extends Fragment {
    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";
    private Handler mHandler = new Handler();
    ListView lvOdgovori;
    TextView tekstPitanja;
    public static Kviz odabraniKviz;
    public ArrayList<Pitanje> pitanjaKviza = new ArrayList<>();
    public ArrayList<String> odgovoriPitanja = new ArrayList<>();
    public OdgovorAdapter adapterOdgovora;

    public static int brojOdgovorenih=0;
    public static int brojTacnih=0;
    public static int brojPreostalih=0;
    public static float procenatTacnih=100;

    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;

    private OnFragmentInteractionListener mListener;

    public PitanjeFrag() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @param param1 Parameter 1.
     * @param param2 Parameter 2.
     * @return A new instance of fragment PitanjeFrag.
     */
    // TODO: Rename and change types and number of parameters
    public static PitanjeFrag newInstance(String param1, String param2) {
        PitanjeFrag fragment = new PitanjeFrag();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_pitanje, container, false);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        lvOdgovori = getView().findViewById(R.id.odgovoriPitanja);
        tekstPitanja = getView().findViewById(R.id.tekstPitanja);
        if(IgrajKvizAkt.kviz!=null){
            odabraniKviz = IgrajKvizAkt.kviz;

            pitanjaKviza = odabraniKviz.getPitanja();
            odabraniKviz.setPitanja(new ArrayList(pitanjaKviza.subList(0,pitanjaKviza.size()-1)));

            pitanjaKviza = odabraniKviz.dajRandomPitanja();
            if(pitanjaKviza.size()!=0){
//                InformacijeFrag.refresh();
                tekstPitanja.setText(pitanjaKviza.get(brojOdgovorenih).getNaziv());
                odgovoriPitanja = pitanjaKviza.get(brojOdgovorenih).dajRandomOdgovore();
                adapterOdgovora = new OdgovorAdapter(getContext(),odgovoriPitanja,pitanjaKviza.get(brojOdgovorenih).getTacan());
                adapterOdgovora.setJelOznacen(false);
                lvOdgovori.setAdapter(adapterOdgovora);
                lvOdgovori.invalidate();
                lvOdgovori.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        adapterOdgovora.setJelOznacen(true);
                        adapterOdgovora.setOdabrani(adapterOdgovora.getItem(position).toString());
                        adapterOdgovora.notifyDataSetChanged();
                        lvOdgovori.invalidate();
                        lvOdgovori.refreshDrawableState();
                        if(adapterOdgovora.getItem(position).toString().equals(pitanjaKviza.get(brojOdgovorenih).getTacan()))brojTacnih++;
                        brojOdgovorenih++;
                        InformacijeFrag.refresh();
                        lvOdgovori.setEnabled(false);
                        mHandler.postDelayed(new Runnable() {
                            @Override
                            public void run() {

                                //brojOdgovorenih++;

                                if(brojOdgovorenih>=pitanjaKviza.size()){
                                    tekstPitanja.setText("Kviz je završen!");
                                    ArrayList<String> praznaLista = new ArrayList<>();
                                    ArrayAdapter<String> prazniOdgovoriAdapter = new ArrayAdapter<>(getContext(),android.R.layout.simple_list_item_1,praznaLista);
                                    brojTacnih=0;
                                    brojOdgovorenih=0;
                                    procenatTacnih=100;

                                    lvOdgovori.setAdapter(prazniOdgovoriAdapter);
                                    prazniOdgovoriAdapter.notifyDataSetChanged();
                                }
                                else{
                                    tekstPitanja.setText(pitanjaKviza.get(brojOdgovorenih).getNaziv());
                                    odgovoriPitanja = pitanjaKviza.get(brojOdgovorenih).dajRandomOdgovore();
                                    adapterOdgovora = new OdgovorAdapter(getContext(),odgovoriPitanja,pitanjaKviza.get(brojOdgovorenih).getTacan());
                                    adapterOdgovora.setJelOznacen(false);
                                    lvOdgovori.setAdapter(adapterOdgovora);
                                    adapterOdgovora.notifyDataSetChanged();
                                    lvOdgovori.refreshDrawableState();
                                    lvOdgovori.invalidateViews();

                                }
                                lvOdgovori.setEnabled(true);
                            }
                        },2000);
                    }
                });
            }
            else{
                tekstPitanja.setText("Ovaj kviz nema pitanja");
            }
        }
        else{
            tekstPitanja.setText("Greska");
        }
    }
    /*
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
