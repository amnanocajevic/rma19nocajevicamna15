package ba.unsa.etf.rma.klase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;
import java.util.List;

import ba.unsa.etf.rma.R;

public class OdgovorAdapter extends ArrayAdapter {
    private Context mContext;
    private String tacan;
    private ArrayList<String> odgovori;
    private boolean jelOznacen=false;
    private String odabrani;
    TextView naziv;
    IconView ikona;
    int resourceLayout;

    public OdgovorAdapter(@NonNull Context context, @SuppressLint("SupportAnnotationUsage") @LayoutRes ArrayList<String> list,String tacan) {
        super(context,0, list);
        mContext = context;
        this.odgovori = list;
        this.tacan = tacan;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        String odgovor = odgovori.get(position); // uzima jedan po jedan kviz

        if (v == null) {
            v = LayoutInflater.from(mContext).inflate(R.layout.customlistelement, parent, false);
        }
        naziv = v.findViewById(R.id.nameL); // mjesto gdje ce nam se ispisati naziv odgvoora
        ikona = v.findViewById(R.id.iconL); // mjesto gdje ce biti ikona odgovora
        ikona.setImageResource(R.drawable.slika1);
        naziv.setText(odgovor);
        if(jelOznacen){
          if(odgovor.equals(tacan)) {
              v.setBackgroundColor(getContext().getResources().getColor(R.color.zelena));
              odgovori.get(position);//cek kontam haha
          }

          else if(!odgovor.equals(tacan) && odgovor.equals(odabrani)){
              v.setBackgroundColor(getContext().getResources().getColor(R.color.crvena));
          }
          else{
              v.setBackgroundColor(getContext().getResources().getColor(R.color.element_pozadina));
          }//
        }
        else{
            v.setBackgroundColor(getContext().getResources().getColor(R.color.element_pozadina));
        }

        return v;
    }

    public void setJelOznacen(boolean jelOznacen) {
        this.jelOznacen = jelOznacen;
    }
    public void setOdabrani(String odabrani) {
        this.odabrani = odabrani;
    }
}
