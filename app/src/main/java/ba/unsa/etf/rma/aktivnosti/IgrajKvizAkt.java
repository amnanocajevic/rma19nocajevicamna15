package ba.unsa.etf.rma.aktivnosti;

import android.app.FragmentTransaction;
import android.content.Intent;
import android.provider.AlarmClock;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Toast;

import java.util.Calendar;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.fragmenti.DetailFrag;
import ba.unsa.etf.rma.fragmenti.InformacijeFrag;
import ba.unsa.etf.rma.fragmenti.ListaFrag;
import ba.unsa.etf.rma.fragmenti.PitanjeFrag;
import ba.unsa.etf.rma.fragmenti.RangLista;
import ba.unsa.etf.rma.klase.Kviz;

public class IgrajKvizAkt extends AppCompatActivity {
    InformacijeFrag gornjiFrag;
    PitanjeFrag donjiFrag;
    RangLista drugiDonjiFrag;
    public static Kviz kviz=new Kviz("",null,null);;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_igraj_kviz_akt);
        Bundle extras = getIntent().getExtras();
        if(extras!=null){
            kviz = extras.getParcelable("kviz");
        }

        int brPitanja = kviz.getPitanja().size()-1;
        final Calendar c = Calendar.getInstance();
        int hour = c.get(Calendar.HOUR_OF_DAY);
        int minute = c.get(Calendar.MINUTE);

        if(brPitanja/2 == 0) {
            Toast.makeText(getApplicationContext(), "Kviz nema pitanja!", Toast.LENGTH_SHORT).show();
        }

        //ovdje se zaokruzuje vrijednost sata
        double doubleVarVrijeme = (double) brPitanja/2;
        int intVarVrijeme = (int) (doubleVarVrijeme + 0.5);

        //provjerava da li je ilegalno vrijeme
        if(minute+intVarVrijeme>=60 && brPitanja/2!=0) {
            Intent openNewAlarm = new Intent(AlarmClock.ACTION_SET_ALARM);
            openNewAlarm.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
            openNewAlarm.putExtra(AlarmClock.EXTRA_HOUR, hour+1);
            openNewAlarm.putExtra(AlarmClock.EXTRA_MINUTES, minute+intVarVrijeme-60);
            startActivity(openNewAlarm);
        } else {
            Intent openNewAlarm = new Intent(AlarmClock.ACTION_SET_ALARM);
            openNewAlarm.putExtra(AlarmClock.EXTRA_SKIP_UI, true);
            openNewAlarm.putExtra(AlarmClock.EXTRA_HOUR, hour);
            openNewAlarm.putExtra(AlarmClock.EXTRA_MINUTES, minute+intVarVrijeme);
            startActivity(openNewAlarm);
        }

        gornjiFrag = new InformacijeFrag();
            donjiFrag = new PitanjeFrag();
            drugiDonjiFrag = new RangLista();

            android.support.v4.app.FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.informacijePlace,donjiFrag);
            ft.replace(R.id.pitanjePlace,gornjiFrag);
            ft.commit();

    }
}
