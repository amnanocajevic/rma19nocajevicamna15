package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

public class CustomAdapterSpinnerMain extends ArrayAdapter<Kategorija> {

    private List<Kategorija> objects;

    public CustomAdapterSpinnerMain(Context context, int textViewResourceId, List<Kategorija> objects, int hidingItemIndex) {
        super(context, textViewResourceId, objects);
        this.objects = objects;
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (position == objects.size()-1) {
            TextView tv = new TextView(getContext());
            tv.setVisibility(View.GONE);
            v = tv;
        } else {
            v = super.getDropDownView(position, null, parent);
        }
        return v;
    }

}
