package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.List;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.aktivnosti.DodajKategorijuAkt;
import ba.unsa.etf.rma.aktivnosti.DodajPitanjeAkt;

public class CustomAdapterOdgovor extends ArrayAdapter<Odgovor> {
    TextView naziv;
    IconView ikona;
    private int resourceLayout;
    private Context mContext;

    public CustomAdapterOdgovor(Context context, int resource, List<Odgovor> items) {
        super(context, resource, items);
        this.resourceLayout = resource;
        this.mContext = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        Odgovor odgovor = getItem(position); // uzima jedan po jedan kviz

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        naziv = v.findViewById(R.id.nameL); // mjesto gdje ce nam se ispisati naziv odgvoora
        ikona = v.findViewById(R.id.iconL); // mjesto gdje ce biti ikona odgovora
        if(odgovor.isTacan())
            v.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.valid));
        else
            v.setBackgroundColor(ContextCompat.getColor(getContext(), R.color.element_pozadina));


        naziv.setText(odgovor.getTekstOdgovora());
        ikona.setImageResource(R.drawable.slika1);

        return v;
    }
}
