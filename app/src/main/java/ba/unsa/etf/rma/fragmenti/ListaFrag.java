package ba.unsa.etf.rma.fragmenti;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.ArrayList;

import ba.unsa.etf.rma.R;
import ba.unsa.etf.rma.klase.Kategorija;
import ba.unsa.etf.rma.klase.KategorijaAdapter;

import static ba.unsa.etf.rma.aktivnosti.KvizoviAkt.kategorije;


public class ListaFrag extends Fragment {

    ListaFragOnItemClickListener listener;
    ListView listaKategorija;
    KategorijaAdapter kategorijaAdapter;

    public interface ListaFragOnItemClickListener {
        void onClickElementListeSent(String nazivElementa);
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.lista_frag, container, false);
        return view;
    }
    public ArrayList<Kategorija> kategorijas;
    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        listaKategorija = getView().findViewById(R.id.listaKategorija);
        kategorijas = new ArrayList<>();
        kategorijas = new ArrayList(kategorije.subList(0,kategorije.size()-1));
        kategorijaAdapter = new KategorijaAdapter(getActivity(),R.layout.customlistelement, kategorijas);
        listaKategorija.setAdapter(kategorijaAdapter);

        listaKategorija.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
               String nazivKategorije = kategorijaAdapter.getItem(position).toString();
               DetailFrag.osvjeziGridView(nazivKategorije);
             }
         });

    }
    /*7         pa jel se instancira uopste jebo ga ja
    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if(context instanceof ListaFragOnItemClickListener)
            listener = (ListaFragOnItemClickListener) context;
        else
            throw new RuntimeException(context.toString() + " treba implementovati ListaFragOnItemClickListener");
    }*/

    @Override
    public void onDetach() {
        super.onDetach();
        listener = null;
    }
}
