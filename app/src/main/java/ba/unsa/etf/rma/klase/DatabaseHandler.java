package ba.unsa.etf.rma.klase;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.annotation.Nullable;

public class DatabaseHandler extends SQLiteOpenHelper {

    //osnovno
    SQLiteDatabase database;
    private static final int DATABASE_VERSION = 1;
    private static final String DATABASE_NAME = "Kvizovi.db";

    //nazivi tabela
    private static final String TABLE_KVIZOVI = "Kvizovi";
    private static final String TABLE_PITANJA= "Pitanja";
    private static final String TABLE_ODGOVORI = "Odgovori";
    private static final String TABLE_KATEGORIJE= "Kategorije";
    private static final String TABLE_RANGLISTE= "Rangliste";

    //kategorije : id naziv
    //kvizovi: id naziv id_kategorije
    //pitanja id tekst id_kviza
    //odgovori id tekst jel tacan id_pitanja
    //rang liste

    public DatabaseHandler(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        //KATEGORIJA, ODGOVOR, PITANJE, KVIZ
        db.execSQL("create table " + TABLE_KATEGORIJE + " (NAZIV_KAT TEXT)");
        db.execSQL("create table " + TABLE_KVIZOVI + " (NAZIV_KVIZ TEXT, ID_KATEGORIJE INTEGER REFERENCES "+ TABLE_KATEGORIJE+ "(rowid))");
        //webserver_id integer REFERENCES target_dp(id)
        db.execSQL("create table " + TABLE_PITANJA + " (TEKST_PITANJA TEXT, ID_KVIZA INTEGER REFERENCES "+TABLE_KVIZOVI+"(rowid))");
        db.execSQL("create table " + TABLE_ODGOVORI + " (TEKST_ODG TEXT, JEL_TACAN INTEGER, ID_PITANJA INTEGER REFERENCES "+TABLE_PITANJA+"(rowid))");
        db.execSQL("create table " + TABLE_RANGLISTE + " (NICKNAME TEXT, SCORE INTEGER)");
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    public boolean insertKviz(String kategorija, String naziv) {
        database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAZIV_KVIZ", naziv);
        contentValues.put("ID_KATEGORIJE",findKategorija(kategorija));
        long result = database.insert(TABLE_KVIZOVI,null,contentValues);
        if(result==-1)
            return false;
        return true;
    }

    public boolean insertPitanje(String tekst, String kviz) {
        database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("TEKST_PITANJA", tekst);
        contentValues.put("ID_KVIZA",findKviz(kviz));
        long result = database.insert(TABLE_PITANJA,null,contentValues);
        if(result==-1)
            return false;
        return true;

    }

    public boolean insertOdgovor(String tekstOdgovora, boolean jelTacan, String pitanje) {
        database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("TEKST_ODG", tekstOdgovora);
        if(jelTacan)
            contentValues.put("JEL_TACAN", 1);
        else
            contentValues.put("JEL_TACAN", 0);
        contentValues.put("ID_PITANJA",findPitanje(pitanje));
        long result = database.insert(TABLE_ODGOVORI,null,contentValues);
        if(result==-1)
            return false;
        return true;

    }

    public boolean insertKategorija(String nazivKategorije) {
        database = getWritableDatabase();
        ContentValues contentValues = new ContentValues();
        contentValues.put("NAZIV_KAT", nazivKategorije);
        long result = database.insert(TABLE_KATEGORIJE,null,contentValues);
        if(result==-1)
            return false;
        return true;
    }

    public boolean insertRanglista() {
        return true;
    }


    public int findKviz (String imeKviza) {
        database = getReadableDatabase();
        Cursor mCursor = database.rawQuery(
                "SELECT rowid  FROM  "+TABLE_KVIZOVI+" WHERE NAZIV_KVIZ= '"+imeKviza+"'" , null);

        return mCursor.getInt(mCursor.getColumnIndex("ID"));

    }


    public int findKategorija (String imeKategorije) {
        database = getReadableDatabase();
        Cursor mCursor = database.rawQuery(
                "SELECT rowid  FROM  "+TABLE_KATEGORIJE+" WHERE NAZIV_KAT= '"+imeKategorije+"'" , null);

        return mCursor.getInt(mCursor.getColumnIndex("ID"));
    }


    public int findPitanje (String tekstPitanja) {
        database = getReadableDatabase();
        Cursor mCursor = database.rawQuery(
                "SELECT rowid  FROM  "+TABLE_PITANJA+" WHERE TEKST_PITANJA = '"+tekstPitanja+"'" , null);

        return mCursor.getInt(mCursor.getColumnIndex("ID"));

    }


    public boolean findOdgovor () {
        return true;
    }


    //nisam sigurna treba li mi ovo tho
    public boolean findRang () {
        return true;
    }

}
