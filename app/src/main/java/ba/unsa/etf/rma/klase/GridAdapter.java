package ba.unsa.etf.rma.klase;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.maltaisn.icondialog.IconView;

import java.util.ArrayList;
import java.util.Objects;

import ba.unsa.etf.rma.R;

public class GridAdapter extends BaseAdapter {

    IconView ikonaKategorije;
    TextView nazivKviza;
    TextView brojPitanja;

    private Context mContext;
    private ArrayList<Kviz> kvizovi;
    private int resourceLayout;

    public GridAdapter(Context mContext,  int resource, ArrayList<Kviz> kvizovi) {
        this.mContext = mContext;
        this.resourceLayout = resource;
        this.kvizovi = kvizovi;
    }

    @Override
    public int getCount() {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        View v = convertView;
        Kviz kviz =(Kviz)getItem(position); // uzima jedan po jedan kviz

        if (v == null) {
            LayoutInflater vi;
            vi = LayoutInflater.from(mContext);
            v = vi.inflate(resourceLayout, null);
        }

        nazivKviza = v.findViewById(R.id.gvTvNazivKviza); // mjesto gdje ce nam se ispisati naziv kviza
        ikonaKategorije = v.findViewById(R.id.gvIconKategorija); // mjesto gdje ce biti ikona kviza
        brojPitanja = v.findViewById(R.id.gvTvBrojPitanja);

        if(!Objects.equals(kviz.getNaziv(), "Dodaj kviz") && kviz!=null)  {
            nazivKviza.setText(kviz.getNaziv());
            ikonaKategorije.setIcon(Integer.parseInt(kviz.getKategorija().getId()));
            brojPitanja.setText(kviz.getPitanja().size());
        }

        return v;
    }
    public void osvjeziAdapter(ArrayList<Kviz> kvizovi) {
        this.kvizovi = kvizovi;
    }
}
